package com.kase.api.model;

import com.kase.api.dto.AbstractDto;
import lombok.Data;

import javax.persistence.*;
import javax.validation.constraints.NotNull;

/**
 * JPA объект книги
 */
@Data
@Entity
@Table(name="BOOK")
public class Book extends IdEntity{

    /**
     * уникальный номер книжного издания, необходимый для распространения книги в торговых сетях и автоматизации работы с изданием
     */
    @Column(name = "isbn", nullable = false, unique = true)
    private String isbn;
    /**
     *  название книги
     */
    @Column(name = "name", nullable = false)
    private String name;
    /**
     * стоимость
     */
    @Column(name = "price", nullable = false)
    private Double price;
    /**
     * тираж
     */
    @Column(name = "count", nullable = false)
    private Integer count;

    @ManyToOne(fetch = FetchType.EAGER, cascade = CascadeType.ALL)
    @JoinColumn(name = "author_id")
    private Author author;
}
