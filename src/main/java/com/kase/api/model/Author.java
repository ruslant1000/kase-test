package com.kase.api.model;

import lombok.Data;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

/**
 * JPA объект автора книги
 */
@Data
@Entity
@Table(name = "AUTHOR", uniqueConstraints = {@UniqueConstraint(columnNames = {"first_name","last_name"})})
public class Author extends IdEntity{

    /**
     * Имя автора
     */
    @Column(name = "first_name", nullable = false)
    private String firstName;

    /**
     * Фамилия автора
     */
    @Column(name = "last_name", nullable = false)
    private String lastName;

    /**
     * Страна автора.
     * Тут можно было бы выделить страну в отдельную таблицу, но тогда надо делать дополнительные REST методы для работы со списком стран
     */
    @Column(name = "country")
    private String country;
}
