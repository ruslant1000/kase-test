package com.kase.api.model;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Calendar;

/**
 * Родительский класс для всех JPA объектов.
 *
 */
@MappedSuperclass
@Data
public class IdEntity implements Serializable {

    @Id
    @GeneratedValue(
            strategy = GenerationType.IDENTITY
    )
    @Column(
            name = "id"
    )
    private Long id;

    /**
     * Дата создания записи
     */
    @Column(
            updatable = false
    )
    @Temporal(TemporalType.TIMESTAMP)
    @JsonFormat(
            shape = JsonFormat.Shape.STRING,
            pattern = "yyyy-MM-dd HH:mm:ss",
            locale = "en_US"
    )
    private Calendar createdDate;

    /**
     * Дата редактирования записи
     */
    @Temporal(TemporalType.TIMESTAMP)
    @JsonFormat(
            shape = JsonFormat.Shape.STRING,
            pattern = "yyyy-MM-dd HH:mm:ss",
            locale = "en_US"
    )
    private Calendar modificatedDate;

    /**
     * Тут можно еще добавить поле версионности для контроля блокировок записей
     */

    @PreUpdate
    protected void preUpdate() {
        this.modificatedDate = Calendar.getInstance();
    }

    @PrePersist
    protected void prePersist() {
        this.createdDate = Calendar.getInstance();
        this.modificatedDate = Calendar.getInstance();
    }

}
