package com.kase.api.exception;

/**
 * Специальный класс исключений, который будем считать нашим штатным исключением
 */
public class KaseException extends Exception{

    public KaseException() {
    }

    public KaseException(String message) {
        super(message);
    }

    public KaseException(String message, Throwable cause) {
        super(message, cause);
    }

    public KaseException(Throwable cause) {
        super(cause);
    }

    public KaseException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
