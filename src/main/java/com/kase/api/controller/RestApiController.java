package com.kase.api.controller;

import com.fasterxml.jackson.annotation.JsonView;
import com.kase.api.dto.AbstractDto;
import com.kase.api.dto.AuthorDto;
import com.kase.api.dto.BookDto;
import com.kase.api.exception.KaseException;
import com.kase.api.service.KaseService;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;
import java.util.Map;

/**
 * REST контроллер для описания REST методов
 */
@RestController
@RequestMapping("/api")
public class RestApiController {

    private KaseService kaseService;

    public RestApiController(KaseService kaseService) {
        this.kaseService = kaseService;
    }

    /**
     * Добавление нового автора в базу
     * @param author
     * @return
     * @throws KaseException
     */
    @PostMapping("/author/create")
    public ResponseEntity create(@RequestBody @Validated(AuthorDto.Create.class) AuthorDto author)throws KaseException {
        Map<String,Object> responce = new HashMap<>();
        responce.put("code",0);
        responce.put("id",kaseService.create(author));
        return new ResponseEntity(responce, HttpStatus.OK);
    }

    /**
     * Добавление новой кники в базу
     * @param book
     * @return
     * @throws KaseException
     */
    @PostMapping("/book/create")
    public ResponseEntity create(@RequestBody @Validated(BookDto.Create.class) BookDto book)throws KaseException {
        kaseService.create(book);
        Map<String,Object> responce = new HashMap<>();
        responce.put("code",0);
        return new ResponseEntity(responce, HttpStatus.OK);
    }

    /**
     * Получение списка добавленных в базу книг определенного автора
     * @param authorId - ID автора книг
     * @return
     * @throws KaseException
     */
    @GetMapping("/books")
    @JsonView(BookDto.ForList.class) // JsonView - используем для того чтобы не показывать клиениту id книги и id автора
    public ResponseEntity books(Long authorId)throws KaseException {
        return new ResponseEntity(kaseService.books(authorId), HttpStatus.OK);
    }
}
