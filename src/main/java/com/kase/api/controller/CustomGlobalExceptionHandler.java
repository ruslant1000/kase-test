package com.kase.api.controller;

import com.kase.api.exception.KaseException;
import org.springframework.core.Ordered;
import org.springframework.core.annotation.Order;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;
import java.util.HashMap;
import java.util.Map;

/**
 * Дополнительный обработчик ошибок, позволяющий форматировать ответы клиенту
 */
@Order(Ordered.HIGHEST_PRECEDENCE)
@ControllerAdvice
public class CustomGlobalExceptionHandler extends ResponseEntityExceptionHandler {


    @ExceptionHandler(KaseException.class)
    public ResponseEntity customHandleKaseException(KaseException ex, WebRequest request) throws Exception {
        Map<String, Object> responce = new HashMap<>();
        responce.put("code",1);
        responce.put("errorMessage", ex.getMessage());

        // тут можно дописать различные обработчики известных ошибок.
        // Например если на каком-либо уровне стэк-трейса исключения обнаружится что-то вроде Constraint Violation Exception,
        // то можно сделать на этом акцент.

        return new ResponseEntity<>(responce, HttpStatus.BAD_REQUEST);



    }

}
