package com.kase.api.config;

import com.kase.api.dto.BookDto;
import com.kase.api.model.Book;
import org.modelmapper.ModelMapper;
import org.modelmapper.PropertyMap;
import org.modelmapper.convention.MatchingStrategies;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * Конфигуратор маппера моделей
 */
@Configuration
public class ModeMapperConfig {

    @Bean
    public ModelMapper modelMapper() {
        ModelMapper mapper = new ModelMapper();
        mapper.getConfiguration()
                .setMatchingStrategy(MatchingStrategies.STRICT)
                .setFieldMatchingEnabled(true)
                .setSkipNullEnabled(true)
                .setFieldAccessLevel(org.modelmapper.config.Configuration.AccessLevel.PRIVATE);


        {
            // маппинг объекта JPA в объект DTO
            PropertyMap<Book, BookDto> propertyMap = new PropertyMap<Book, BookDto>() {
                @Override
                protected void configure() {
                    map().setAuthorId(source.getAuthor().getId());
                    map().setAuthorFirstName(source.getAuthor().getFirstName());
                    map().setAuthorLastName(source.getAuthor().getLastName());
                    map().setAuthorCountry(source.getAuthor().getCountry());
                }
            };
            mapper.typeMap(Book.class, BookDto.class).addMappings(propertyMap);
        }

        return mapper;
    }
}
