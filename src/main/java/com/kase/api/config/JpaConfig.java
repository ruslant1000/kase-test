package com.kase.api.config;

import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

/**
 * Spring-Data Конфигуратор
 */
@Configuration
@EnableJpaRepositories(basePackages = {"com.kase.api.repository"})
@EntityScan(basePackages = {"com.kase.api.model"})
public class JpaConfig{
}
