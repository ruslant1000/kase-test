package com.kase.api.dto;

import com.fasterxml.jackson.annotation.JsonView;
import lombok.Data;

import javax.validation.constraints.NotNull;

/**
 * данные по книге
 */
@Data
public class BookDto extends AbstractDto{

    public static interface ForList {}

    /**
     * уникальный номер книжного издания, необходимый для распространения книги в торговых сетях и автоматизации работы с изданием
     */
    @NotNull(groups = {Create.class})
    @JsonView(ForList.class)  // JsonView - используем для того чтобы в некоторых случаях отфильтровать поля для клиента
    private String isbn;
    /**
     *  название книги
     */
    @NotNull(groups = {Create.class})
    @JsonView(ForList.class)
    private String name;
    /**
     * стоимость
     */
    @NotNull(groups = {Create.class})
    @JsonView(ForList.class)
    private Double price;
    /**
     * тираж
     */
    @NotNull(groups = {Create.class})
    @JsonView(ForList.class)
    private Integer count;
    /**
     * id автора
     */
    @NotNull(groups = {Create.class})
    private Long authorId;

    @JsonView(ForList.class)
    private String authorFirstName;

    @JsonView(ForList.class)
    private String authorLastName;

    @JsonView(ForList.class)
    private String authorCountry;


    @Override
    public String toString() {
        return "BookDto{" +
                "isbn='" + isbn + '\'' +
                ", name='" + name + '\'' +
                ", price=" + price +
                ", count=" + count +
                ", authorId=" + authorId +
                '}';
    }
}
