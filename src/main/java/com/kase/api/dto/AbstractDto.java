package com.kase.api.dto;

import lombok.Data;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Null;

/**
 * Родительский класс, для определения общих параметров и методов для всех наследуемых DTO объектов
 */
@Data
public class AbstractDto {

    public static interface Create{}
    public static interface Update{}

    @Null(groups = {Create.class})
    @NotNull(groups = {Update.class})
    private Long id;
}
