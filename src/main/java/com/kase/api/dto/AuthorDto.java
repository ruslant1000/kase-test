package com.kase.api.dto;

import lombok.Data;

import javax.validation.constraints.NotNull;

/**
 * DTO объект содержащий данные об авторе
 */
@Data
public class AuthorDto extends AbstractDto{

    /**
     *
     * Если при определении какого-либо POST-метода
     * класс AuthorDto будет указан в качестве основного тела запроса и обязательно помечен аннотацией @Validated(AuthorDto.Create.class),
     * то в таком случае аннотация @NotNull имеет смысл и будет контролировать вызов POST-метода, сообщая пользователю пропущенных
     * обазятельных входных параметрах.
     */
    @NotNull(groups = {Create.class, Update.class})
    private String firstName;

    @NotNull(groups = {Create.class, Update.class})
    private String lastName;


    private String country;

    @Override
    public String toString() {
        return "AuthorDto{" +
                "firstName='" + firstName + '\'' +
                ", lastName='" + lastName + '\'' +
                ", country='" + country + '\'' +
                '}';
    }
}
