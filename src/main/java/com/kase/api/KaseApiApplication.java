package com.kase.api;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication

public class KaseApiApplication {

    public static void main(String[] args) {
        SpringApplication.run(KaseApiApplication.class, args);
    }

}
