package com.kase.api.service;

import com.kase.api.dto.AuthorDto;
import com.kase.api.dto.BookDto;
import com.kase.api.exception.KaseException;
import com.kase.api.model.Author;
import com.kase.api.model.Book;
import com.kase.api.repository.AuthorRepository;
import com.kase.api.repository.BookRepository;
import org.modelmapper.ModelMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import java.util.LinkedList;
import java.util.List;
import java.util.Optional;


@Service
public class KaseService {

    private static Logger log = LoggerFactory.getLogger(KaseService.class);

    private AuthorRepository authorRepository;
    private BookRepository bookRepository;

    private ModelMapper modelMapper;

    public KaseService(AuthorRepository authorRepository, ModelMapper modelMapper, BookRepository bookRepository) {
        this.authorRepository = authorRepository;
        this.modelMapper = modelMapper;
        this.bookRepository=bookRepository;
    }

    public Long create(AuthorDto dto)throws KaseException {
        try{
            log.debug("Creating new author "+dto.toString());
            Author author = new Author();
            author.setFirstName(dto.getFirstName());
            author.setLastName(dto.getLastName());
            author.setCountry(dto.getCountry());
            author = authorRepository.save(author);
            log.debug("created new author");
            return author.getId();
        }catch (Exception e){
            log.error("Error creating new author",e);
            throw new KaseException("Error creating new author. "+(e.getMessage()!=null?e.getMessage():""),e);

        }
    }

    public Long create(BookDto dto)throws KaseException {
        try{
            log.debug("Creating new book "+dto.toString());

            Optional<Author> author = authorRepository.findById(dto.getAuthorId());
            if(!author.isPresent())
                throw new Exception("Not found author with id "+dto.getAuthorId());

            Book book = new Book();
            book.setAuthor(author.get());
            book.setCount(dto.getCount());
            book.setIsbn(dto.getIsbn());
            book.setName(dto.getName());
            book.setPrice(dto.getPrice());



            book = bookRepository.save(book);
            log.debug("created new book");
            return book.getId();
        }catch (Exception e){
            log.error("Error creating new book",e);
            throw new KaseException("Error creating new book. "+(e.getMessage()!=null?e.getMessage():""),e);

        }
    }

    public List<BookDto> books(Long authorId)throws KaseException{
        try{
            log.debug("getting author's books. authorId: "+authorId+"...");

            Optional<Author> author = authorRepository.findById(authorId);
            if(!author.isPresent())
                throw new Exception("Not found author with id "+authorId);

            List<Book> books = bookRepository.findAllByAuthorId(authorId);
            if(books==null || books.size()==0)
                throw new Exception("The data not found");

            List<BookDto> list = new LinkedList<>();

            for(Book book:books){
                list.add(modelMapper.map(book, BookDto.class));
            }

            return list;
        }catch (Exception e){
            log.error("Error getting author's books",e);
            throw new KaseException("Error getting author's books. "+(e.getMessage()!=null?e.getMessage():""),e);

        }
    }

}
