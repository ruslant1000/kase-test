package com.kase.api.repository;

import com.kase.api.model.Book;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface BookRepository extends JpaRepository<Book,Long> {

    public List<Book> findAllByAuthorId(Long authorId);
}
