##Краткая документация созданного API


### Сохранение автора книги
POST http://localhost:8080/api/author/create
Content-Type: application/json

{
  "firstName": "Ruslan",
  "lastName": "Temirbulatov",
  "country": "Kazakhstan"
}
Поля в теле JSON запроса:
 firstName - имя. Поле обязательное
 lastName - фамилия. Поле обязательное
 country - страна. Поле НЕ обязательное

### в случае успешности выполнения операции будет получен положительный ответ 
{
  "code": 0,  
  "id": 5
}

Поля в теле JSON ответа:

code = 0 - признак успешности выполнения операции

id - идентификатор созданной записи

### в случае отрицательного результата выполнения операции будет получен отрицательный ответ 
{
  "code": 1,
  "errorMessage": "Error creating new author. could not execute statement; SQL [n/a]; constraint [uksyl6q0w50pg4cboumv0gse20t]; nested exception is org.hibernate.exception.ConstraintViolationException: could not execute statement"
}

Поля в теле JSON ответа:

code = 1 - признак отрицательного результата выполнения операции

errorMessage - детальное описание ошибки

### Сохранение книги
POST http://localhost:8080/api/book/create
Content-Type: application/json

{
  "isbn": "0000011",
  "name": "Хакеры сновидений",
  "price": 100.0,
  "count": 1000,
  "authorId": 1
}
### в случае успешности выполнения операции будет получен положительный ответ
{
  "code": 0
}

### в случае отрицательного результата выполнения операции будет получен отрицательный ответ 
{
  "code": 1,
  "errorMessage": "Error creating new book. could not execute statement; SQL [n/a]; constraint [uk_ehpdfjpu1jm3hijhj4mm0hx9h]; nested exception is org.hibernate.exception.ConstraintViolationException: could not execute statement"
}

### Получение списка книг по авторам книг.
GET http://localhost:8080/api/books?authorId=1
Content-Type: application/json

### в случае успешности выполнения операции будет получен положительный ответ

 [
   {
     "isbn": "000001",
     "name": "Хакеры сновидений",
     "price": 100.0,
     "count": 1000,
     "authorFirstName": "Ruslan",
     "authorLastName": "Temirbulatov",
     "authorCountry": "Kazakhstan"
   },
   ...
 ]
 
Поля в теле JSON ответа:

isbn - уникальный номер книжного издания, необходимый для распространения книги в торговых сетях и автоматизации работы с изданием

name - название

price - стоимость

count - тираж

authorFirstName - имя автора
 
authorLastName - фамилия автора

authorCountry - страна автора


###----------------------
Тест кейсы не писал, вместо этого использовал scratch файл (/src/main/resources/http/rest-api.http) для проверки работоспособности методов.

